#include "fifo.h"

static qItem *head;
static qItem *tail;

void initFifo(void)
{
    head = NULL;
}

static qItem *NEW(void *data)
{
    qItem *tmp = NULL;

    tmp = malloc(sizeof(*tmp));
    tmp->next = NULL;
    tmp->data = data;
    return tmp;
}

void putFifo(void *data)
{
    qItem *tmp = NULL;

    if(!head)
    {
        head = (tail = NEW(data));
        return;
    }
    tail->next = NEW(data);
    tail = tail->next;
}

void *getFifo(void)
{
    void *data = head->data;
    qItem *next = head->next;

    free(head);
    head = next;

    return data;
}

int isFifoEmpty(void)
{
    return NULL == head;
}



