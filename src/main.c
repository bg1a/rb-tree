#include "main.h"

/**
 * Allocate memory for a new node
 * @param: key, a node key
 * @return node pointer
 */
node *allocNode(char key)
{
    node *tmp;

    tmp = malloc(sizeof(*tmp));
    memset(tmp, 0, sizeof(*tmp));
    tmp->color = 'r';
    tmp->key = key;
    return tmp;
}

/**
 * Add a new node to the tree
 * @param: head, an address of a head node pointer
 * @param: key, a node key
 */
void insertNode(node **head, char key)
{
    node **cur = head;

    /* printf("Key is: %c\n", key); */
    while(*cur)
    {
        if(key < (*cur)->key)
        {
            cur = &(*cur)->left;
        }else if(key > (*cur)->key){
            cur = &(*cur)->right;
        }else{
            break;
        }
    }
    if(!*cur)
    {
        *cur = allocNode(key);
    }else if(key == (*cur)->key){
        /* key is found */
    }
}

/**
 * Performs a right rotation
 * @param: head, a node pointer
 * @return a pointer a new head
 */
node *rotR(node *head)
{
    node *leftNode = head->left;

/*      node           leave
 *     /    \   ->    /     \
 *  leave                  node
 */
    leftNode->color = head->color;
    head->color = 'r';
    head->left = leftNode->right;
    leftNode->right = head;
    return leftNode;
}

/**
 * Performs a left rotation:
 * @param: head, a node pointer
 * @return a new head pointer
 */
node *rotL(node *head)
{
    node *rightNode = head->right;
/*
 *       node             leave
 *      /    \     ->    /     \
 *          leave      node
 */
    rightNode->color = head->color;
    head->color = 'r';
    head->right = rightNode->left;
    rightNode->left = head;
    return rightNode;
}

/**
 * When a node is added to a tree, this function
 * is balancing the tree
 * @param: head, an address of head node pointer
 */
void balanceTree(node **head)
{
    node **linkAddr;
    (*head)->color = 'b';

    /*
     * Push all nodes to a stack. So, leaves will be extracted
     * first of all.
     */
    initStack();
    pushStack(head);
    while(!isEndStack())
    {
        linkAddr = (node **)currentStack();
        if((*linkAddr)->left)
            pushStack(&(*linkAddr)->left);
        if((*linkAddr)->right)
            pushStack(&(*linkAddr)->right);
        nextStack();
    }

    /*
     * Get nodes from a stack and check their links.
     * If both links are red, so change a color of the current node.
     * If right link is red, always perform a left rotation, so that
     * we will avoid some 4-node configuration, like:
     *       node
     *       /   \red
     *   leave   node
     *          /    \red
     *               leave
     * Finally, check the left-left configuration and rotate it.
     */
    node *lnk;
    while(linkAddr = popStack())
    {
        lnk = *linkAddr;
        if(lnk->left && lnk->right)
        {
            if(('r' == lnk->left->color) && ('r' == lnk->right->color))
            {
                lnk->color = 'r';
                lnk->left->color = 'b';
                lnk->right->color = 'b';
            }
        }
        if(lnk->right)
        {
            if('r' == lnk->right->color)
                *linkAddr = rotL(lnk);
        }
        if(lnk->left && lnk->left->left)
        {
            if(('r' == lnk->left->color) && ('r' == lnk->left->left->color))
            {
                *linkAddr = rotR(lnk);
            }
        }
        /*
         * This check is left for debug issues
         *
        if(lnk->left && lnk->right)
        {
            if(('r' == lnk->left->color) && ('r' == lnk->right->color))
            {
                lnk->color = 'r';
                lnk->left->color = 'b';
                lnk->right->color = 'b';
            }
        }
        */
    }


    /*
    node **tmp = NULL;
    printf("keys from stack:\n");
    for(tmp = popStack(); tmp && (*tmp); tmp = popStack())
    {
        printf("key: %c\n", (*tmp)->key);
    }
*/

}

/**
 * Prints a tree graph to a *.dot file
 * @param: head, a pointer to a tree head node
 */
void printTree(node *head)
{
    FILE *dot;
    node *tmp = NULL;
    initFifo();
    dot = fopen(DOT_FILE, "w");
    fprintf(dot, "strict graph m{\n");

    putFifo((void *)head);
    while(!isFifoEmpty())
    {
        tmp = (node*)getFifo();
        if(tmp->left)
        {
            fprintf(dot, "\t%c -- %c %s;\n", tmp->key, tmp->left->key,
                    tmp->left->color == 'r' ? "[color=red]":"[color=black]");
            putFifo((void *)tmp->left);
        }
        if(tmp->right)
        {
            fprintf(dot, "\t%c -- %c %s;\n", tmp->key, tmp->right->key,
                    tmp->right->color == 'r' ? "[color=red]":"[color=black]");
            putFifo((void *)tmp->right);
        }
    }

    fprintf(dot, "}\n");
    fclose(dot);
}

int main(int argc, char **argv)
{
#define TEST_STR "ASEARCHINGX"
    node *head = NULL;
    char test[] = TEST_STR;


    int i;
    for(i = 0; i < sizeof(TEST_STR) - 1; i++)
    {
        insertNode(&head, test[i]);
        balanceTree(&head);
    }
    printTree(head);
    system(VIS_SCRIPT);

#if 0
    /*
     * Another input method. You will get an image of a tree after a
     * single node is added.
     *
     */
    char ch;
    int cnt;
    for(cnt = 0, ch = 0; cnt < 10; cnt++, ch = 0)
    {
        while(!isalpha(ch))
            scanf("%c", &ch);
        insertNode(&head, ch);
        balanceTree(&head);
        printTree(head);
        system(VIS_SCRIPT);
    }
#endif

    return 0;
}
