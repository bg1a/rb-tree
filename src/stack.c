#include "stack.h"

static sItem *head;
static sItem *cur;

void initStack(void)
{
    cur = (head = NULL);
}

static sItem * NEW(void *data)
{
    sItem *tmp = NULL;

    tmp = malloc(sizeof(*tmp));
    tmp->next = NULL;
    tmp->prev = NULL;
    tmp->data = data;
    return tmp;
}

void pushStack(void *data)
{
    if(!head)
    {
        cur = (head = NEW(data));
        return;
    }
    head->prev = NEW(data);
    head->prev->next = head;
    head = head->prev;
}

void *popStack(void)
{
    void *data = NULL;
    sItem *tmp = NULL;
    if(head)
    {
        data = head->data;
        tmp = head->next;
        free(head);
        if(tmp)
            tmp->prev = NULL;

        head = tmp;
    }
    return data;
}

/**
 * Move a stack pointe
 */
void nextStack(void)
{
    cur = cur->prev;
}

/**
 * Checks if a stack pointer has reached an end of stack
 * @return NULL, if stack pointer has reached the end
 */
int isEndStack(void)
{
    return NULL == cur;
}

/**
 * Get a value at the current stack pointer
 * @return a pointer to the stored data
 */
void *currentStack(void)
{
    return cur->data;
}
