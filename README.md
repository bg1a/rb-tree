RB-tree
=======

Overview
--------
Left-leaning RB tree implementation.

Details
-------
Was tested at Ubuntu 14.04.
Run *make* and *tree* to get an image of a RB tree.
By default it uses a test sequence: "ASEARCHINGX". However, you may use another
input method in main.c.
The main idea of the implementation is to use a stack instead of recursion.
When a new key is added to the tree, a balancing routine is invoked. It pushes
nodes to the stack starting with the head node. At the top of the stack there
will be leaves only. Balancing function checks each node left and right link.
When right is 'red' rotation to the left is always performed (left-leaning RB tree).
It helps to avoid several configurations of nodes in a 4-node.


