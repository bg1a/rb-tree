#ifndef __RB_STACK__
#define __RB_STACK__

#include <stdlib.h>

typedef struct sItem_{
    void *data;
    struct sItem_ *next;
    struct sItem_ *prev;
}sItem;

void initStack(void);
void pushStack(void *data);
void *popStack(void);
void nextStack(void);
int isEndStack(void);
void *currentStack(void);

#endif /* __RB_STACK__ */
