#ifndef __RB_MAIN__
#define __RB_MAIN__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "fifo.h"
#include "stack.h"

typedef struct node_{
    char key;
    char color;
    struct node_ *left;
    struct node_ *right;
}node;

void printTree(node *head);
void balanceTree(node **head);
node *rotL(node *head);
node *rotR(node *head);
void insertNode(node **head, char key);
node *allocNode(char key);

#endif /* __RB_MAIN__ */
