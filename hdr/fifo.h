#ifndef __RB_FIFO__
#define __RB_FIFO__

#include <stdlib.h>

typedef struct qItem_{
    void *data;
    struct qItem_ *next;
}qItem;
void initFifo(void);
void putFifo(void *data);
void *getFifo(void);
int isFifoEmpty(void);

#endif /* __RB_FIFO__ */
