include config

PWD := $(shell pwd)
SRC_DIR = $(PWD)/src
HDR_DIR = $(PWD)/hdr
SRC = $(wildcard $(SRC_DIR)/*.c)
HDR = $(wildcard $(HDR_DIR)/*.h)
INC = $(HDR_DIR)
CC := gcc
BIN := tree
DEBUG :=
#DEBUG := -g3 -gdwarf-2

all:
	$(CC) $(DEBUG) -I$(INC) -DDOT_FILE=\"$(PWD)/$(DOT_FILE)\" -DVIS_SCRIPT=\"$(PWD)/graph.sh\" -o $(BIN) $(SRC)

clean:
	rm -f $(PWD)/$(BIN)
	rm -f $(PWD)/$(DOT_FILE)
	rm -f $(PWD)/$(IMG_FILE).$(IMG_FMT)
